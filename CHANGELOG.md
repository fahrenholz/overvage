# CHANGELOG

<!--- next entry here -->

## 0.3.0
2020-09-30

### Features

- **ci:** now with working download links (ee9be9b76c8ac50492b03809dc6aef1a502d9c30)

## 0.2.0
2020-09-30

### Features

- **tool:** add darwin and linux binaries (a07425c7967db7b55f0dd92511ec22d97767d062)

## 0.1.0
2020-09-30

### Features

- **tool:** initial release (0d7f5c4c7c5e4776639fe764f7eaf5d7368c01a8)

### Fixes

- **ci:** fix path of release-binary (fa45fb05f9e4c1da2d99205c6522c0e8a2f3a011)
- **ci:** next version is now properly determined (6c1b036c30cca966955017269e1f9dd762f09049)
- **ci:** variable GSG_INITIAL_DEVELOPMENT is now set to 'true' instead of 'yes' (64e31a23491c5e916f84157c91f537cc26cfa69f)