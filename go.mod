module gitlab.com/fahrenholz/overvage

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate/v4 v4.12.2
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gosimple/slug v1.9.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/jordan-wright/email v0.0.0-20200602115436-fd8a7622303e
	github.com/lib/pq v1.8.0 // indirect
	github.com/markbates/pkger v0.15.1
	github.com/rs/zerolog v1.19.0
	github.com/sethvargo/go-envconfig v0.2.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/fahrenholz/testutils v0.0.0-20200703190431-5fbab7a0b068
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
