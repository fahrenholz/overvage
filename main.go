package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	_ "github.com/joho/godotenv/autoload"
	"github.com/markbates/pkger"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/sethvargo/go-envconfig"
	"github.com/urfave/cli/v2"
	"gitlab.com/fahrenholz/overvage/internal/pkg/dispatch"
	"gitlab.com/fahrenholz/overvage/internal/pkg/notify"
	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

const gracefulPeriodSeconds = 25

// Config represents the main configuration of overvage.
type Config struct {
	DB           *storage.DatabaseConfig
	Logger       *LoggerConfig
	Web          *dispatch.WebConfig
	Email        *notify.EmailConfig
	Notification *notify.NotificationConfig
}

// LoggerConfig represents the logging-configuration of overvage.
type LoggerConfig struct {
	Level string `env:"LOG_LEVEL,default=INFO"`
}

//nolint:gochecknoglobals
var version = "dev"

func main() {
	app := &cli.App{
		Name:      "Overvåge",
		Usage:     "A low-cost-tool to supervise your servers, services and other possibly failing stuff.",
		UsageText: "",
		Version:   version,
		Action:    overvage,
		Authors: []*cli.Author{
			{
				Name:  "Vincent Fahrenholz",
				Email: "me@fahrenholz.eu",
			},
		},
		Copyright: "",
	}

	_ = app.Run(os.Args)
}

func overvage(mctx *cli.Context) error {
	referenceAssets()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	ctx, cancel := context.WithCancel(mctx.Context)

	defer cancel()

	termChan := make(chan os.Signal, 1)

	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)

	var c Config
	if err := envconfig.Process(ctx, &c); err != nil {
		log.Error().Err(err).Msg("invalid configuration")

		return fmt.Errorf("[Main]: %w", err)
	}

	defer log.Info().Msg("bye!")

	var wg sync.WaitGroup

	db, err := storage.NewDatabase(*c.DB)
	if err != nil {
		log.Error().Err(err).Msg("can't create database connection")
	}

	checks := make(chan notify.CheckEvent)
	notifier := notify.NewEmailNotifier(*c.Email)

	//nolint:gomnd
	wg.Add(2)

	go dispatch.ListenWeb(ctx, &wg, c.Web.Port, c.Web.UIMode, db, checks)

	go notify.NotificationDispatcher(ctx, &wg, c.Notification.UpdatePeriod, db, checks, notifier)

	log.Info().Msg("overvage is fully operational")

	<-termChan
	cancel()

	done := make(chan struct{})

	go func() {
		wg.Wait()
		close(done)
	}()

	select {
	case <-done:
		log.Info().Msg("graceful shutdown of goroutines successfully ended")
	case <-time.After(gracefulPeriodSeconds * time.Second):
		log.Info().Msg("some goroutines are still running, will now shut down anyway.")
	}

	_ = db.Close()

	return nil
}

func referenceAssets() {
	//nolint:staticcheck
	pkger.Include("/db")
}
