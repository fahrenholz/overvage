![Overvåge](docs/overvage_logo.png "Overvåge")

A low-cost-tool to supervise your servers, services and other possibly failing stuff.

## What can I do with this?

This tool might be for you if:
* you have some infrastructure or processes you want to monitor
* you have no budget for enterprise monitoring tools
* you are perfectly fine with only getting an email on a check-failure
* you want a simple red-green overview of your current application-state

It's fairly easy described: `POST` something to Overvåge via the API (you can find an OpenAPI-Spec 
[here](api/openapi.yml) to guide you). This has to contain the definition of your check as well as its result and all 
notification information. Overvåge will _only_ handle the notification and the visual display of your current check status. 
It doesn't even care what you are checking.

### Real life example

I have a website, a mail server and a cron deleting some garbage on the webserver. For the website, I want to monitor 
if the site is accessible. For the mail server, I want to monitor that my SSL-certificate is not due to renew. For the 
cron, I want to monitor that it is executed regularly. So, I build a script on my mail server checking, via cron, that 
my website is responding to http requests and if my certificate is up to date. In both cases I send the result to the 
Overvåge API. In my cron, at the end of its run, I send a request to the Overvåge API, telling basically "I've done things".

## Installation

You have two ways of running Overvåge:
* install the release-binary matching your system and run it <!-- TODO: add unit-file, installation step-by-step -->
* run it via docker <!-- TODO: add example, dockerfile, push to docker-hub -->

## Configuration

Configuration is done via env-vars:

| Name | Required | Default | Possible values | Description |
| ---- | -------- | ------- | --------------- | ----------- |
| WEB_PORT | | `9000` | integer | Port the web listener will attach itself on. Please keep in mind that you need rood for some privileged ports. |
| UI_MODE | | `light` | `light` or `dark` | UI color scheme. |
| DB_TYPE | X | | `in_memory` or `mysql` | Type of backing database. According to your type, some other config variables may become required. |
| DB_HOST | in case of DB_TYPE is `mysql` | | string | Your database host and port. |
| DB_NAME | in case of DB_TYPE is `mysql` | | string | Your database name. |
| DB_USERNAME | in case of DB_TYPE is `mysql` | | string | Your database username. |
| DB_PASSWORD | in case of DB_TYPE is `mysql` | | string | Your database password. |
| LOG_LEVEL | | `INFO` | `ERROR`, `WARN`, `INFO`, `DEBUG` | The log level of the application. |
| EMAIL_HOST | X | | string | Hostname of your SMTP-Server. |
| EMAIL_PORT | | `587` | integer | Port of your SMTP-Server. |
| EMAIL_SENDER | X | | string | From-Address for outgoing notifications. |
| EMAIL_SENDERNAME | | `Overvåge` | Name which will be set as your sender-name. |
| EMAIL_USER | X | | string | Username which Overvåge will use to send emails. |
| EMAIL_PASSWORD | X | | string | Password which Overvåge will use to connect to your SMTP-Server. |
| NOTIFICATION_UPDATE_AFTER | | `60m` | time.Duration-parseable string | Period of time which Overvåge will wait before re-notifying on a still failing check. |

### Security

There is no built-in way of protecting the API or restricting access, nor will there be in near future. 
If you need restrictions, please use a reverse proxy with some authentication mechanism.
<!-- TODO: add example -->

## Usage

* submit a check result by sending a POST-request to `/api/checks`.
* See your results by navigating to `/` in your browser.

## License

Overvåge is published under MIT-license. See [LICENSE](./LICENSE) for more details.
