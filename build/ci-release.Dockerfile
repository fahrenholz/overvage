FROM golang:1.15-alpine
RUN set -e; \
    apk add --no-cache make git; \
    go get github.com/markbates/pkger/cmd/pkger;