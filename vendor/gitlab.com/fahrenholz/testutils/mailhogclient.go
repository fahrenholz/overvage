package testutils

import (
    "encoding/json"
    "errors"
    "fmt"
    "io/ioutil"
    "net/http"
    "time"
)

type MailhogApiClient struct {
    BaseUri string
    Username string
    Password string
}

func NewMailhogApiClient (baseUri string, username string, password string) *MailhogApiClient {
    return &MailhogApiClient{
        BaseUri:  baseUri,
        Username: username,
        Password: password,
    }
}

func (c *MailhogApiClient) GetMessages() (MailhogMessageCollection,error) {
    var coll MailhogMessageCollection
    cl := http.Client{}
    req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", c.BaseUri, "/api/v2/messages"), nil)
    if err != nil {
        return coll, err
    }
    req.SetBasicAuth(c.Username, c.Password)
    r, err := cl.Do(req)
    if err != nil {
        return coll, err
    }
    defer func() { _ = r.Body.Close() }()
    jsonStr, err := ioutil.ReadAll(r.Body)
    if err != nil {
        return coll, err
    }
    err = json.Unmarshal(jsonStr, &coll)
    if err != nil {
        return coll, err
    }
    return coll, nil
}

func (c *MailhogApiClient) Flush() error{
    cl := http.Client{}
    req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s%s", c.BaseUri, "/api/v1/messages"), nil)
    if err != nil {
        return err
    }
    req.SetBasicAuth(c.Username, c.Password)
    r, err := cl.Do(req)
    defer func() { _ = r.Body.Close() }()
    if r.StatusCode != 200 {
        return errors.New("flush didn't work")
    }
    return nil
}

type MailhogMessageCollection struct {
    TotalMessages int `json:"total"`
    Offset int `json:"start"`
    Limit int `json:"count"`
    Messages []MailhogMessage `json:"items"`
}

type MailhogMessage struct {
    Id   string      `json:"ID"`
    From MailhogPath `json:"From"`
    To []MailhogPath `json:"To"`
    Content MailhogContent `json:"Content"`
    Created time.Time `json:"Created"`
    Mime string `json:"MIME"`
    Raw MailhogRawMessage `json:"Raw"`
}

type MailhogPath struct {
    Relays []string `json:"Relays"`
    Mailbox string `json:"Mailbox"`
    Domain string `json:"Domain"`
    Params string `json:"Params"`
}

type MailhogContent struct {
    Headers map[string][]string `json:"Headers"`
    Body string `json:"Body"`
    Size int `json:"Size"`
    Mime string `json:"MIME"`
}

type MailhogRawMessage struct {
    From string `json:"From"`
    To []string `json:"To"`
    Data string `json:"Data"`
    Helo string `json:"Helo"`
}