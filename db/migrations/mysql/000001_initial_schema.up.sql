CREATE TABLE IF NOT EXISTS overvage_checks (
    name VARCHAR(255) PRIMARY KEY,
    label VARCHAR(255),
    type VARCHAR(255),
    description TEXT,
    priority INT(11),
    last_update TIMESTAMP,
    referrer VARCHAR(255),
    success TINYINT(1)
)character set UTF8 collate utf8_bin;