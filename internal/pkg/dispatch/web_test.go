package dispatch

import (
	"bytes"
	"context"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/rs/zerolog"
	"gitlab.com/fahrenholz/overvage/internal/pkg/notify"
	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

const (
	succeed = "\u2713"
	failed  = "\u2717"
)

func init() {
	zerolog.SetGlobalLevel(zerolog.Disabled)
}

func TestJsonContentMiddleware(t *testing.T) {
	hF := jsonContentTypeMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)

	t.Log("should create handler that sets Content-Type-Header to 'application/json' when executed")

	hF.ServeHTTP(w, r)

	if w.Header().Get("Content-Type") != "application/json" {
		t.Errorf("\t%s: content-type should be 'application/json': %s", failed, w.Header().Get("Content-Type"))
	} else {
		t.Logf("\t%s: content-type should be 'application/json'.", succeed)
	}

	_ = r.Body.Close()
}

func TestStaticFileRoutes(t *testing.T) {
	t.Log("should be able to deliver a static file, with all cache-headers set")

	req, err := http.NewRequest("GET", "/static/img/favicon.ico", nil)
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not create request: %v", failed, err)
	}

	rr := httptest.NewRecorder()
	db := storage.NewInMemoryDatabase()

	tpl, err := getParsedTemplate("light")
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not create template: %v", failed, err)
	}

	ch := make(chan notify.CheckEvent)
	router := getRouter(db, tpl, ch)

	router.ServeHTTP(rr, req)

	close(ch)

	if rr.Code != 200 {
		t.Errorf("\t%s: should return status code 200: %d", failed, rr.Code)
	} else {
		t.Logf("\t%s: should return status code 200.", succeed)
	}

	if rr.Header().Get("Cache-Control") == "" {
		t.Errorf("\t%s: should set a cache-control-header: no header set.", failed)
	} else {
		t.Logf("\t%s: should set a cache-control-header.", succeed)
	}

	if rr.Header().Get("Etag") == "" {
		t.Errorf("\t%s: should set an etag: no etag set.", failed)
	} else {
		t.Logf("\t%s: should set an etag.", succeed)
	}
}

func TestStatusAliveRoute(t *testing.T) {
	t.Log("should be able to deliver status/alive-response ok with status-code 200")

	req, err := http.NewRequest("GET", "/status/alive", nil)
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not create request: %v", failed, err)
	}

	rr := httptest.NewRecorder()
	db := storage.NewInMemoryDatabase()

	tpl, err := getParsedTemplate("light")
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not create template: %v", failed, err)
	}

	ch := make(chan notify.CheckEvent)
	router := getRouter(db, tpl, ch)
	router.ServeHTTP(rr, req)
	// END
	close(ch)

	if rr.Code != http.StatusOK {
		t.Errorf("\t%s: should return status code 200: %d", failed, rr.Code)
	} else {
		t.Logf("\t%s: should return status code 200.", succeed)
	}
}

func TestGetChecksRoute(t *testing.T) {
	t.Log("should be able to deliver /-response ok with status-code 200 and the adequate amount of checks in it")

	n := time.Now().Round(time.Second).UTC()
	exp1 := storage.CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := storage.CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}
	vals := []struct {
		name string
		set  []storage.CheckResult
	}{
		{"no-set", nil},
		{"set-1", []storage.CheckResult{exp1}},
		{"set-2", []storage.CheckResult{exp1, exp2}},
	}

	for _, tt := range vals {
		t.Run(tt.name, func(t *testing.T) {
			// BOILERPLATE
			req, err := http.NewRequestWithContext(context.TODO(), "GET", "/", nil)
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not create request: %v", failed, err)
			}

			rr := httptest.NewRecorder()
			db := storage.NewInMemoryDatabase()

			for _, v := range tt.set {
				_ = db.UpsertCheck(v)
			}

			tpl, err := getParsedTemplate("light")
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not create template: %v", failed, err)
			}

			ch := make(chan notify.CheckEvent)

			router := getRouter(db, tpl, ch)
			router.ServeHTTP(rr, req)

			close(ch)

			if rr.Code != http.StatusOK {
				t.Errorf("\t%s: should return status code 200: %d", failed, rr.Code)
			} else {
				t.Logf("\t%s: should return status code 200.", succeed)
			}

			doc, _ := goquery.NewDocumentFromReader(rr.Body)

			cnt := doc.Find(".check").Length()
			if cnt != len(tt.set) {
				t.Errorf("\t%s: should find %d checks: %d", failed, len(tt.set), cnt)
			} else {
				t.Logf("\t%s: should find %d checks.", succeed, len(tt.set))
			}
		})
	}
}

func TestPostCheckRoute(t *testing.T) {
	t.Log("should be able to respond an error on non-json-body or create the checks and notification events otherwise")

	vals := []struct {
		name       string
		json       string
		expErr     bool
		expEventNb int
	}{
		{"error", `*iam-invalid{"]`, true, 0},
		{"success-no-notify-given", `{"label": "test", "type": "test", "priority": 1, "success": true}`, false, 0},
		{"success-with-notify-given", `{"label": "test", "type": "test", "priority": 1, "success": true, "notify_email": ["test@example.com"]}`, false, 1},
		{"failure-no-notify-given", `{"label": "test", "type": "test", "priority": 1, "success": false}`, false, 0},
		{"failure-with-notify-given", `{"label": "test", "type": "test", "priority": 1, "success": false, "notify_email": ["test@example.com"]}`, false, 1},
	}

	for _, tt := range vals {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequestWithContext(context.TODO(), "POST", "/api/checks", bytes.NewReader([]byte(tt.json)))
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not create request: %v", failed, err)
			}

			rr := httptest.NewRecorder()
			db := storage.NewInMemoryDatabase()

			tpl, err := getParsedTemplate("light")
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not create template: %v", failed, err)
			}

			ch := make(chan notify.CheckEvent, 1)

			router := getRouter(db, tpl, ch)
			router.ServeHTTP(rr, req)

			close(ch)

			var events []notify.CheckEvent

			for v := range ch {
				events = append(events, v)
			}

			if tt.expErr && rr.Code != http.StatusBadRequest {
				t.Errorf("\t%s: should get status code 400: %d", failed, rr.Code)

				return
			} else if tt.expErr {
				t.Logf("\t%s: should get status code 400", succeed)

				return
			}

			if rr.Code != http.StatusCreated {
				t.Errorf("\t%s: should get status code 201: %d", failed, rr.Code)
			} else {
				t.Logf("\t%s: should get status code 201.", succeed)
			}

			checks, _ := db.GetChecks()
			if len(checks) != 1 {
				t.Errorf("\t%s should add one check in database: %d", failed, len(checks))
			} else {
				t.Logf("\t%s: should add one check in database", succeed)
			}

			if len(events) != tt.expEventNb {
				t.Errorf("\t%s: should generate %d events: %d", failed, tt.expEventNb, len(events))
			} else {
				t.Logf("\t%s: should generate %d events.", succeed, tt.expEventNb)
			}
		})
	}
}

func TestGetReferrerFromRequest(t *testing.T) {
	vals := []struct {
		name string
		req  http.Request
		exp  string
	}{
		{"in-x-header", http.Request{RemoteAddr: "wrong", Header: map[string][]string{"X-Forwarded-For": {"right"}}}, "right"},
		{"in-RemoteAddr", http.Request{RemoteAddr: "right"}, "right"},
	}

	t.Log("should return referrer from request appropriatedly")

	for _, tt := range vals {
		t.Run(tt.name, func(t *testing.T) {
			res := getReferrerFromRequest(&tt.req)
			if res != tt.exp {
				t.Errorf("\t%s: expect referrer to be '%s': %s", failed, tt.exp, res)
			} else {
				t.Logf("\t%s: expect referrer to be '%s'", succeed, tt.exp)
			}
		})
	}
}

func TestListenWebShutdownMechanisms(t *testing.T) {
	// WARNING: this test should be run with `timeout`-flag. If something does not work, it can run indefinitely
	t.Log("should be able to spawn webserver, listen to a dedicated port for one resource and shut down when context is canceled")

	db := storage.NewInMemoryDatabase()
	ctx, cancel := context.WithCancel(context.TODO())

	var wg sync.WaitGroup

	wg.Add(1)

	ch := make(chan notify.CheckEvent, 1)

	go ListenWeb(ctx, &wg, 43000, "dark", db, ch)

	time.Sleep(2000 * time.Microsecond)

	req, _ := http.NewRequestWithContext(context.TODO(), "GET", "http://localhost:43000/", nil)
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		t.Fatalf("\t%s: http-query to the web listener should not throw any error: %s", failed, err)
	} else {
		t.Logf("\t%s: http-query to the web listener should not throw any error.", succeed)
	}

	defer func() { _ = resp.Body.Close() }()

	if resp.StatusCode != 200 {
		t.Errorf("\t%s: http-query to the web listener should provide status 200: %d", failed, resp.StatusCode)
	} else {
		t.Logf("\t%s: http-query to the web listener should provide status 200.", succeed)
	}

	cancel()
	wg.Wait()
	t.Logf("\t%s: cancel-func should be able to shut down the listener", succeed)
}
