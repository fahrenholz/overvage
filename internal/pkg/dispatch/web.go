package dispatch

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gosimple/slug"
	"github.com/markbates/pkger"
	"github.com/rs/zerolog/log"
	"gitlab.com/fahrenholz/overvage/internal/pkg/notify"
	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

const webListenerShutdownTime = 15

// WebConfig represents the web-specific configuration of overvage.
type WebConfig struct {
	Port   int    `env:"WEB_PORT,default=9000"`
	UIMode string `env:"UI_MODE,default=light"`
}

// ListenWeb starts the web-server listening to incoming API-requests and delivering the UI.
func ListenWeb(ctx context.Context, wg *sync.WaitGroup, listenerPort int, uiMode string, db storage.Database, checks chan notify.CheckEvent) {
	defer log.Debug().Msg("web listener is now gone")
	defer wg.Done()

	log.Debug().Int("port", listenerPort).Str("uiMode", uiMode).Msg("web listener starts")

	t, err := getParsedTemplate(uiMode)
	if err != nil {
		log.Error().Err(err).Msg("could not get parsed template")

		return
	}

	r := getRouter(db, t, checks)
	srv := &http.Server{Addr: fmt.Sprintf(":%d", listenerPort), Handler: r}

	var serverShutdown sync.WaitGroup

	serverShutdown.Add(1)

	go func() {
		defer log.Debug().Msg("web listener is not available anymore")
		defer serverShutdown.Done()

		if err := srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			log.Fatal().Err(err).Msg("web listener didn't survive")
		}
	}()

	<-ctx.Done()

	shutdownContext, tcancel := context.WithTimeout(context.Background(), webListenerShutdownTime*time.Second)

	defer tcancel()

	log.Debug().Msg("web listener has gracefully stopped all operations")

	_ = srv.Shutdown(shutdownContext) // this tries to terminate the server gracefully for 15 seconds.

	serverShutdown.Wait()
}

func getRouter(db storage.Database, t *template.Template, checks chan notify.CheckEvent) *mux.Router {
	r := mux.NewRouter()

	ui := r.PathPrefix("/").Subrouter()
	ui.Handle("/", handlers.CompressHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handleUIIndex(w, db, t)
	})))

	staticFiles := http.FileServer(pkger.Dir("/assets"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", cacheControlWrapper(staticFiles)))

	status := r.PathPrefix("/status").Subrouter()
	status.Use(jsonContentTypeMiddleware)
	status.HandleFunc("/alive", statusAliveHandler)

	api := r.PathPrefix("/api").Subrouter()
	api.Use(jsonContentTypeMiddleware)
	api.Use(mux.CORSMethodMiddleware(api))
	api.HandleFunc("/checks", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		if r.Method == http.MethodOptions {
			return
		}
		handleChecksPost(w, r, db, checks)
	}).Methods(http.MethodPost, http.MethodOptions)

	return r
}

func cacheControlWrapper(files http.Handler) http.Handler {
	// TODO: firefox still doesn't cache it
	etag := base64.StdEncoding.EncodeToString([]byte(time.Now().Format(time.RFC3339Nano)))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "public,max-age=31536000")
		w.Header().Add("Etag", etag)
		files.ServeHTTP(w, r)
	})
}

func jsonContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func statusAliveHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	_, err := w.Write([]byte("{\"status\": \"OK\"}"))
	if err != nil {
		log.Warn().Err(err).Msg("could not write status-alive-body")
	}
}

func handleUIIndex(w http.ResponseWriter, db storage.Database, t *template.Template) {
	checks, err := db.GetChecks()
	if err != nil {
		log.Error().Err(err).Msg("could not load checks")
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	err = t.Execute(w, struct {
		Checks []storage.CheckResult
	}{checks})
	if err != nil {
		log.Error().Err(err).Msg("could not write template")
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	log.Info().Msg("ui delivered")
}

func getParsedTemplate(uiMode string) (*template.Template, error) {
	//nolint:staticcheck
	pkger.Include("/web")

	tmplFileName := "index_light.tmpl"

	if strings.ToLower(uiMode) == "dark" {
		tmplFileName = "index_dark.tmpl"
	}

	// Load file from pkpger virtual file, or real file if pkged.go has not
	// yet been generated, during development.
	tmplFile, err := pkger.Open(fmt.Sprintf("/web/%s", tmplFileName))
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	snpFile, err := pkger.Open("/web/snippets.tmpl")
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	// Now read it.
	contentString, err := ioutil.ReadAll(snpFile)
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	templateString, err := ioutil.ReadAll(tmplFile)
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	tmpl := template.New("check_overview")

	tmpl, err = tmpl.Parse(string(contentString))
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	tmpl, err = tmpl.Parse(string(templateString))
	if err != nil {
		return nil, fmt.Errorf("[Dispatch] %w", err)
	}

	return tmpl, nil
}

func handleChecksPost(w http.ResponseWriter, r *http.Request, db storage.Database, checks chan notify.CheckEvent) {
	const error500Msg = "something unexpected happened. Please try again later. If the problem persists, please contact the administrator"

	jsonCheck, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error().Err(err).Msg("could not get body")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(error500Msg))

		return
	}

	var check storage.CheckResult

	err = json.Unmarshal(jsonCheck, &check)
	if err != nil {
		log.Error().Err(err).Msg("could not decode body: wrong format")
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(fmt.Sprintf("could not decode body: %s", err.Error())))

		return
	}

	check.Name = slug.Make(check.Label)
	check.LastUpdate = time.Now().UTC()

	if check.Referrer == "" {
		check.Referrer = getReferrerFromRequest(r)
	}

	event, err := notify.DetermineCheckEvent(db, check, jsonCheck)
	if err != nil {
		log.Error().Err(err).Msg("could not be transformed to an event")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(error500Msg))

		return
	}

	err = db.UpsertCheck(check)
	if err != nil {
		log.Error().Err(err).Msg("could not insert into database")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(error500Msg))

		return
	}

	if len(event.Notify) > 0 {
		checks <- event
	}

	w.WriteHeader(http.StatusCreated)
}

func getReferrerFromRequest(r *http.Request) string {
	if r.Header.Get("X-Forwarded-For") == "" {
		return r.RemoteAddr
	}

	return r.Header.Get("X-Forwarded-For")
}
