package notify

import (
	"bytes"
	"context"
	"fmt"
	"mime/quotedprintable"
	"testing"
	"time"

	"github.com/sethvargo/go-envconfig"
	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
	"gitlab.com/fahrenholz/testutils"
)

const (
	exampleDomain  = "example.net"
	exampleMailbox = "overvage"
)

var mc EmailConfig

var hog *testutils.MailhogApiClient

type TestConfig struct {
	MailhogAPIHost  string `env:"MAILHOG_API_HOST,default=http://127.0.0.1:8025"`
	MailhogSMTPHost string `env:"MAILHOG_SMTP_HOSTNAME,default=127.0.0.1"`
}

var tc TestConfig

func init() {
	_ = envconfig.Process(context.TODO(), &tc)
	mc = EmailConfig{
		Host:       tc.MailhogSMTPHost,
		Port:       1025,
		Sender:     "overvage@example.net",
		SenderName: "Overvåge",
		User:       "user",
		Password:   "password",
	}
	hog = testutils.NewMailhogApiClient(tc.MailhogAPIHost, "user", "password")
}

func TestEmailNotifier_SendFailure(t *testing.T) {
	s := NewEmailNotifier(mc)
	values := []struct {
		name           string
		desc           string
		isUpdate       bool
		subjectWording string
	}{
		{name: "first-time", desc: "should be able to send a correct first-time-failure message in case of no update", isUpdate: false, subjectWording: "FAILING: testcheck"},
		{name: "update", desc: "should be able to send a correct update-failure message in case of update", isUpdate: true, subjectWording: "STILL FAILING: testcheck"},
	}

	for _, tt := range values {
		t.Run(tt.name, func(t *testing.T) {
			t.Log(tt.desc)

			_ = hog.Flush()
			now := time.Now()

			s.SendFailure(CheckEvent{EventDate: now, Notify: []string{"test@example.com"}, New: storage.CheckResult{Type: "test", Label: "testcheck", Description: "this is a test", LastUpdate: now, Referrer: "gotest", Success: false}}, tt.isUpdate)

			coll, _ := hog.GetMessages()
			if coll.TotalMessages != 1 {
				t.Errorf("\t%s: should send one message: %d", failed, coll.TotalMessages)
			} else {
				t.Logf("\t%s: should send one message.", succeed)
			}

			if coll.Messages[0].Content.Headers["Subject"][0] != tt.subjectWording {
				t.Errorf("\t%s: should set '%s' as subject: %s", failed, tt.subjectWording, coll.Messages[0].Content.Headers["Subject"][0])
			} else {
				t.Logf("\t%s: should set '%s' as subject.", succeed, tt.subjectWording)
			}

			if coll.Messages[0].From.Mailbox != exampleMailbox || coll.Messages[0].From.Domain != exampleDomain {
				t.Errorf("\t%s: should have from set to 'overvage@example.net': %s", failed, fmt.Sprintf("%s@%s", coll.Messages[0].From.Mailbox, coll.Messages[0].From.Domain))
			} else {
				t.Logf("\t%s: should have from set to 'overvage@example.net'.", succeed)
			}

			if len(coll.Messages[0].To) != 1 {
				t.Errorf("\t%s: should have one recipient: %d", failed, len(coll.Messages[0].To))
			} else {
				t.Logf("\t%s: should have one recipient.", succeed)
			}

			buff := bytes.Buffer{}
			w := quotedprintable.NewWriter(&buff)
			_, _ = w.Write([]byte(fmt.Sprintf("Overvåge has got bad news:\n\nOn: %s\nLabel: testcheck\nType: test\nReferrer: gotest\n\nDescription:\nthis is a test\n\nThe system wishes you luck!", now.Format(time.RFC1123))))
			_ = w.Close()

			if buff.String() != coll.Messages[0].Content.Body {
				t.Errorf("\t%s: Body should be as expected.", failed)
			} else {
				t.Logf("\t%s: Body should be as expected.", succeed)
			}
		})
	}
}

func TestEmailNotifier_SendRecovery(t *testing.T) {
	s := NewEmailNotifier(mc)

	t.Log("should be able to send a correct recovery mail")

	_ = hog.Flush()
	now := time.Now()

	s.SendRecovery(CheckEvent{EventDate: now, Notify: []string{"test@example.com"}, New: storage.CheckResult{Type: "test", Label: "testcheck", Description: "this is a test", LastUpdate: now, Referrer: "gotest", Success: false}})

	coll, _ := hog.GetMessages()
	if coll.TotalMessages != 1 {
		t.Errorf("\t%s: should send one message: %d", failed, coll.TotalMessages)
	} else {
		t.Logf("\t%s: should send one message.", succeed)
	}

	if coll.Messages[0].Content.Headers["Subject"][0] != "RECOVERY: testcheck" {
		t.Errorf("\t%s: should set 'RECOVERY: testcheck' as subject: %s", failed, coll.Messages[0].Content.Headers["Subject"][0])
	} else {
		t.Logf("\t%s: should set 'RECOVERY: testcheck' as subject.", succeed)
	}

	if coll.Messages[0].From.Mailbox != exampleMailbox || coll.Messages[0].From.Domain != exampleDomain {
		t.Errorf("\t%s: should have from set to 'overvage@example.net': %s", failed, fmt.Sprintf("%s@%s", coll.Messages[0].From.Mailbox, coll.Messages[0].From.Domain))
	} else {
		t.Logf("\t%s: should have from set to 'overvage@example.net'.", succeed)
	}

	if len(coll.Messages[0].To) != 1 {
		t.Errorf("\t%s: should have one recipient: %d", failed, len(coll.Messages[0].To))
	} else {
		t.Logf("\t%s: should have one recipient.", succeed)
	}

	buff := bytes.Buffer{}
	w := quotedprintable.NewWriter(&buff)
	_, _ = w.Write([]byte("Check has recovered:\n\nLabel: testcheck\nType: test\nReferrer: gotest\n\nDescription:\nthis is a test\n\nWell done!"))
	_ = w.Close()

	if buff.String() != coll.Messages[0].Content.Body {
		t.Errorf("\t%s: Body should be as expected.", failed)
	} else {
		t.Logf("\t%s: Body should be as expected.", succeed)
	}
}

func TestEmailNotifier_SendNoUpdate(t *testing.T) {
	s := NewEmailNotifier(mc)

	t.Log("should be able to send a correct no-update mail")

	_ = hog.Flush()
	now := time.Now()

	s.SendNoUpdate(CheckEvent{EventDate: now, Notify: []string{"test@example.com"}, New: storage.CheckResult{Type: "test", Label: "testcheck", Description: "this is a test", LastUpdate: now, Referrer: "gotest", Success: false}})

	coll, _ := hog.GetMessages()
	if coll.TotalMessages != 1 {
		t.Errorf("\t%s: should send one message: %d", failed, coll.TotalMessages)
	} else {
		t.Logf("\t%s: should send one message.", succeed)
	}

	if coll.Messages[0].Content.Headers["Subject"][0] != "NO RECENT UPDATE: testcheck" {
		t.Errorf("\t%s: should set 'NO RECENT UPDATE: testcheck' as subject: %s", failed, coll.Messages[0].Content.Headers["Subject"][0])
	} else {
		t.Logf("\t%s: should set 'NO RECENT UPDATE: testcheck' as subject.", succeed)
	}

	if coll.Messages[0].From.Mailbox != exampleMailbox || coll.Messages[0].From.Domain != exampleDomain {
		t.Errorf("\t%s: should have from set to 'overvage@example.net': %s", failed, fmt.Sprintf("%s@%s", coll.Messages[0].From.Mailbox, coll.Messages[0].From.Domain))
	} else {
		t.Logf("\t%s: should have from set to 'overvage@example.net'.", succeed)
	}

	if len(coll.Messages[0].To) != 1 {
		t.Errorf("\t%s: should have one recipient: %d", failed, len(coll.Messages[0].To))
	} else {
		t.Logf("\t%s: should have one recipient.", succeed)
	}

	buff := bytes.Buffer{}
	w := quotedprintable.NewWriter(&buff)
	_, _ = w.Write([]byte(fmt.Sprintf("Overvåge may have detected a problem: There has been no update for the following check since %s.\n\nLabel: testcheck\nType: test\nReferrer: gotest\n\nDescription:\nthis is a test\n\nThe system wishes you luck!", now.Format(time.RFC1123))))
	_ = w.Close()

	if buff.String() != coll.Messages[0].Content.Body {
		t.Errorf("\t%s: Body should be as expected.", failed)
	} else {
		t.Logf("\t%s: Body should be as expected.", succeed)
	}
}
