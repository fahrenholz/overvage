package notify

import (
	"encoding/json"
	"testing"
	"time"

	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

const (
	succeed = "\u2713"
	failed  = "\u2717"
)

func TestCheckResult_DisplayedLastUpdate(t *testing.T) {
	t.Log("should return the RFC-1123-formatted last-update-time of the checkresult")

	now := time.Now()
	r := storage.CheckResult{LastUpdate: now}

	if now.Format(time.RFC1123) != r.DisplayedLastUpdate() {
		t.Errorf("\t%s: should be expected date-string: %s", failed, r.DisplayedLastUpdate())
	} else {
		t.Logf("\t%s: should be expected date-string.", succeed)
	}
}

func TestCheckResult_DisplayedTypeGroup(t *testing.T) {
	values := []struct {
		name     string
		desc     string
		typeName string
		exp      string
	}{
		{"lowercase", "should return any lowercase value as uppercase group", "http_something", "HTTP"},
		{"uppercase", "should return any uppercase value as is", "PING_SERVER", "PING"},
		{"botchered", "should be able to deal with an empty group", "_story", "UNKNOWN"},
		{"botchered_second", "should be able to deal with an empty name", "", "UNKNOWN"},
	}

	for _, tt := range values {
		t.Run(tt.name, func(t *testing.T) {
			t.Log(tt.desc)
			r := storage.CheckResult{Type: tt.typeName}
			if r.DisplayedTypeGroup() != tt.exp {
				t.Errorf("\t%s: type group should be '%s': %s", failed, tt.exp, r.DisplayedTypeGroup())
			} else {
				t.Logf("\t%s: type group should be '%s'", succeed, tt.exp)
			}
		})
	}
}

func TestDetermineCheckEvent(t *testing.T) {
	jsonCheck := `{"label":"testcheck", "type": "test", "description": "", "priority":0, "referrer":"", "success":false, "notify_email":["test@example.com","test@example.net"], "gracetime":"50m", "update_timeout":"60m"}`
	values := []struct {
		name           string
		desc           string
		precedingCheck *storage.CheckResult
	}{
		{"no_old_check", "should be able to get the event when there is no old check", nil},
		{"has_old_check", "should be able to get the event when there is an old check", &storage.CheckResult{Name: "testcheck", Label: "testcheck", Type: "test", LastUpdate: time.Now().Add(-1 * 5 * 24 * time.Hour)}},
	}

	for _, tt := range values {
		t.Run(tt.name, func(t *testing.T) {
			t.Log(tt.desc)

			db := storage.NewInMemoryDatabase()
			if tt.precedingCheck != nil {
				_ = db.UpsertCheck(*tt.precedingCheck)
			}

			r := storage.CheckResult{
				Name:       "testcheck",
				Label:      "testcheck",
				Type:       "test",
				LastUpdate: time.Now(),
			}

			err := json.Unmarshal([]byte(jsonCheck), &r)
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not unmarshal CheckResult: %v", failed, err)
			}

			e, err := DetermineCheckEvent(db, r, []byte(jsonCheck))
			if err != nil {
				t.Fatalf("\t%s: should not get any error: %s", failed, err)
			} else {
				t.Logf("\t%s: should not get any error.", succeed)
			}

			if len(e.Notify) != 2 || (e.Notify[0] != "test@example.com" && e.Notify[1] != "test@example.net") {
				t.Errorf("\t%s: notify should be ['test@example.com','test@example.net']: %v", failed, e.Notify)
			} else {
				t.Logf("\t%s: notify should be ['test@example.com','test@example.net']", succeed)
			}

			if time.Now().Add(47 * time.Minute).After(e.GraceTimeDeadline) {
				t.Errorf("\t%s: gracetimeDeadline should be in roughly 50 minutes: %s", failed, e.GraceTimeDeadline.Format(time.RFC1123))
			} else {
				t.Logf("\t%s: gracetimeDeadline should be in roughly 50 minutes.", succeed)
			}

			if time.Now().Add(57 * time.Minute).After(e.UpdateDeadline) {
				t.Errorf("\t%s: updateDeadline should be in roughly 60 minutes: %s", failed, e.UpdateDeadline.Format(time.RFC1123))
			} else {
				t.Logf("\t%s: updateDeadline should be in roughly 60 minutes.", succeed)
			}

			if r.Name != e.New.Name || r.Label != e.New.Label || r.Success != e.New.Success || r.LastUpdate != e.New.LastUpdate || r.Type != e.New.Type || r.Referrer != e.New.Referrer || r.Description != e.New.Description || r.Priority != e.New.Priority {
				t.Errorf("\t%s: checkresult and checkevent.new should have the same values: %v <=> %v", failed, r, e.New)
			} else {
				t.Logf("\t%s: checkresult and checkevent.new should have the same values", succeed)
			}

			if tt.precedingCheck != nil {
				testWithPrecedingCheck(t, e, tt)
			} else {
				if e.Old.Name != "" {
					t.Errorf("\t%s: checkevent.Old should be zero value: %v", failed, e.Old)
				} else {
					t.Logf("\t%s: checkevent.Old should be zero value.", succeed)
				}
			}
		})
	}
}

func testWithPrecedingCheck(t *testing.T, e CheckEvent, tt struct {
	name           string
	desc           string
	precedingCheck *storage.CheckResult
}) {
	t.Helper()

	if e.Old.Name == "" {
		t.Errorf("\t%s: checkevent.Old should be populated: %v", failed, e.Old)
	} else {
		t.Logf("\t%s: checkevent.Old should be populated.", succeed)
	}

	if tt.precedingCheck.Name != e.Old.Name || tt.precedingCheck.Label != e.Old.Label || tt.precedingCheck.Success != e.Old.Success || tt.precedingCheck.LastUpdate != e.Old.LastUpdate || tt.precedingCheck.Type != e.Old.Type || tt.precedingCheck.Referrer != e.Old.Referrer || tt.precedingCheck.Description != e.Old.Description || tt.precedingCheck.Priority != e.Old.Priority {
		t.Errorf("\t%s: checkevent.Old should be as preceding check definition: %v <-> %v", failed, e.Old, *tt.precedingCheck)
	} else {
		t.Logf("\t%s: checkevent.Old should be as preceding check definition", succeed)
	}
}
