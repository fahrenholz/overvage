package notify

import (
	"fmt"
	"net/smtp"
	"time"

	"github.com/jordan-wright/email"
	"github.com/rs/zerolog/log"
)

const sendingFailed = "email could not be sent"

// EmailConfig represents the smtp-configuration of overvage for the email-notifier.
type EmailConfig struct {
	Host       string `env:"EMAIL_HOST,required"`
	Port       int    `env:"EMAIL_PORT,default=587"`
	Sender     string `env:"EMAIL_SENDER"`
	SenderName string `env:"EMAIL_SENDERNAME,default=Overvåge"`
	User       string `env:"EMAIL_USER,required"`
	Password   string `env:"EMAIL_PASSWORD,required"`
}

// Notifier is the interface every dispatching-system has to implement in order to be usable by overvage.
// It is used to decouple the notification-target-system from the notification mechanism.
type Notifier interface {
	SendFailure(event CheckEvent, isUpdate bool)
	SendNoUpdate(event CheckEvent)
	SendRecovery(event CheckEvent)
}

// EmailNotifier satisfies the Notifier-Interface and can be used to send notifications via email.
type EmailNotifier struct {
	c EmailConfig
}

// NewEmailNotifier returns a ready-to-use EmailNotifier.
func NewEmailNotifier(c EmailConfig) *EmailNotifier {
	// TODO: this should be value-semantics
	return &EmailNotifier{c: c}
}

// SendFailure sends a Failure-Email to the given recipients. As a failure will regularly be reemitted, isUpdate is used to choose if it is the first time the failure gets notified or not.
func (e *EmailNotifier) SendFailure(event CheckEvent, isUpdate bool) {
	m := email.NewEmail()
	m.From = e.getSenderString()
	m.To = event.Notify
	m.Subject = fmt.Sprintf("FAILING: %s", event.New.Label)

	if isUpdate {
		m.Subject = fmt.Sprintf("STILL %s", m.Subject)
	}

	m.Text = []byte(fmt.Sprintf(
		"Overvåge has got bad news:\n\nOn: %s\nLabel: %s\nType: %s\nReferrer: %s\n\nDescription:\n%s\n\nThe system wishes you luck!",
		event.EventDate.Format(time.RFC1123),
		event.New.Label,
		event.New.Type,
		event.New.Referrer,
		event.New.Description,
	))

	err := m.Send(e.getSendParameters())
	if err != nil {
		log.Error().Err(err).Msg(sendingFailed)
	}
}

// SendRecovery sends a recovery-email to the given recipients.
func (e *EmailNotifier) SendRecovery(event CheckEvent) {
	m := email.NewEmail()
	m.From = e.getSenderString()
	m.To = event.Notify
	m.Subject = fmt.Sprintf("RECOVERY: %s", event.New.Label)

	m.Text = []byte(fmt.Sprintf(
		"Check has recovered:\n\nLabel: %s\nType: %s\nReferrer: %s\n\nDescription:\n%s\n\nWell done!",
		event.New.Label,
		event.New.Type,
		event.New.Referrer,
		event.New.Description,
	))

	err := m.Send(e.getSendParameters())
	if err != nil {
		log.Error().Err(err).Msg(sendingFailed)
	}
}

// SendNoUpdate sends an alert, that the check hasn't been updated for a given period of time to the given recipients.
func (e *EmailNotifier) SendNoUpdate(event CheckEvent) {
	m := email.NewEmail()
	m.From = e.getSenderString()
	m.To = event.Notify
	m.Subject = fmt.Sprintf("NO RECENT UPDATE: %s", event.New.Label)
	m.Text = []byte(fmt.Sprintf(
		"Overvåge may have detected a problem: There has been no update for the following check since %s.\n\nLabel: %s\nType: %s\nReferrer: %s\n\nDescription:\n%s\n\nThe system wishes you luck!",
		event.EventDate.Format(time.RFC1123),
		event.New.Label,
		event.New.Type,
		event.New.Referrer,
		event.New.Description,
	))

	err := m.Send(e.getSendParameters())
	if err != nil {
		log.Error().Err(err).Msg(sendingFailed)
	}
}

func (e *EmailNotifier) getSenderString() string {
	res := e.c.Sender

	if e.c.SenderName != "" {
		res = fmt.Sprintf("%s <%s>", e.c.SenderName, e.c.Sender)
	}

	return res
}

func (e *EmailNotifier) getSendParameters() (string, smtp.Auth) {
	return fmt.Sprintf("%s:%d", e.c.Host, e.c.Port), smtp.CRAMMD5Auth(e.c.User, e.c.Password)
}
