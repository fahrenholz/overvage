package notify

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

// NotificationConfig represents the overall notification configuration of overvage.
type NotificationConfig struct {
	UpdatePeriod time.Duration `env:"NOTIFICATION_UPDATE_AFTER,default=60m"`
}

func NotificationDispatcher(ctx context.Context, wg *sync.WaitGroup, updatePeriod time.Duration, db storage.Database, checks chan CheckEvent, notifier Notifier) {
	defer log.Debug().Msg("notification dispatcher gracefully stopped")
	defer wg.Done()

	var notifierWg sync.WaitGroup

	for {
		select {
		case event := <-checks:
			log.Debug().Msg("got event")

			dispatchFunction := chooseDispatch(event)
			if dispatchFunction != nil {
				notifierWg.Add(1)

				go dispatchFunction(ctx, db, event, updatePeriod, &notifierWg, notifier)
			}

		default:
			if ctx.Err() != nil {
				notifierWg.Wait()

				return
			}
		}
	}
}

func chooseDispatch(event CheckEvent) func(ctx context.Context, db storage.Database, event CheckEvent, updatePeriod time.Duration, wg *sync.WaitGroup, notifier Notifier) {
	switch {
	case event.Old.Name == "" && !event.New.Success:
		fallthrough
	case event.Old.Name == event.New.Name && event.Old.Success != event.New.Success && !event.New.Success:
		return dispatchFailureNotification
	case event.New.Success && event.WatchesForUpdates:
		return dispatchNoUpdateNotification
	}

	return nil
}

func dispatchFailureNotification(ctx context.Context, db storage.Database, event CheckEvent, updatePeriod time.Duration, wg *sync.WaitGroup, notifier Notifier) {
	defer log.Debug().Msgf("Failure-Notification goroutine for %s exited", event.New.Name)
	defer wg.Done()

	var sentTime time.Time

	for {
		if time.Now().After(event.GraceTimeDeadline) {
			ev, err := db.GetCheckByName(event.New.Name)
			if err == nil && !event.New.LastUpdate.Equal(ev.LastUpdate) && ev.Success {
				if !sentTime.IsZero() {
					log.Debug().Msg("Recovered-check-notification sent")
					notifier.SendRecovery(event)
				}

				return // there has been an update, check is green again, our job is done here
			}

			if time.Now().After(sentTime.Add(updatePeriod)) {
				log.Debug().Msg("Failed-check-notification sent")
				notifier.SendFailure(event, !sentTime.IsZero())
				sentTime = time.Now()
			}
		}

		if ctx.Err() != nil {
			// TODO: save state in order to recover it
			return
		}

		time.Sleep(updatePeriod)
	}
}

func dispatchNoUpdateNotification(ctx context.Context, db storage.Database, event CheckEvent, updatePeriod time.Duration, wg *sync.WaitGroup, notifier Notifier) {
	defer log.Debug().Msgf("No-Update-Notification goroutine for %s exited", event.New.Name)
	defer wg.Done()

	var lastNotification time.Time

	for {
		ev, err := db.GetCheckByName(event.New.Name)
		if err == nil && !event.New.LastUpdate.Equal(ev.LastUpdate) {
			return // there has been an update, our job is done here
		}

		if time.Now().After(event.UpdateDeadline) && time.Now().After(lastNotification.Add(updatePeriod)) {
			notifier.SendNoUpdate(event)
			log.Debug().Msg("No-update-notification sent")
		}

		if ctx.Err() != nil {
			// TODO: save state in order to recover it (redis seems pretty good as backing persistence I think)
			return
		}

		time.Sleep(updatePeriod)
	}
}
