package notify

import (
	"context"
	"reflect"
	"runtime"
	"sync"
	"testing"
	"time"

	"gitlab.com/fahrenholz/overvage/internal/pkg/storage"
)

// Fake notifier for retrieving notifications in isolation, satisfies Notifier.
type FakeNotifier struct {
	sendings []sending
}

type sending struct {
	sendingType string
	event       CheckEvent
	isUpdate    bool
}

func (fn *FakeNotifier) SendFailure(event CheckEvent, isUpdate bool) {
	fn.sendings = append(fn.sendings, sending{"failure", event, isUpdate})
}

func (fn *FakeNotifier) SendNoUpdate(event CheckEvent) {
	fn.sendings = append(fn.sendings, sending{"noupdate", event, false})
}

func (fn *FakeNotifier) SendRecovery(event CheckEvent) {
	fn.sendings = append(fn.sendings, sending{"recovery", event, false})
}

func (fn *FakeNotifier) CountFailures() (overall, firsts, updates int) {
	for _, v := range fn.sendings {
		if v.sendingType == "failure" {
			overall++

			if v.isUpdate {
				updates++
			} else {
				firsts++
			}
		}
	}

	return
}

func (fn *FakeNotifier) CountNoUpdate() (res int) {
	for _, v := range fn.sendings {
		if v.sendingType == "noupdate" {
			res++
		}
	}

	return
}

func (fn *FakeNotifier) CountRecoveries() (res int) {
	for _, v := range fn.sendings {
		if v.sendingType == "recovery" {
			res++
		}
	}

	return
}

func TestDispatchFailureNotification(t *testing.T) {
	db := storage.NewInMemoryDatabase()
	tn := time.Now()
	cr := storage.CheckResult{Success: false, Name: "test", Label: "test", LastUpdate: tn.Add(-1 * time.Minute)}
	cru := storage.CheckResult{Success: true, Name: "test", Label: "test", LastUpdate: tn.Add(2 * time.Minute)}
	ce := CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: cr, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: tn.Add(5 * time.Minute)}
	fn := FakeNotifier{}
	ctx := context.TODO()

	var wg sync.WaitGroup

	_ = db.UpsertCheck(cr)

	t.Log("should be able to generate multiple notification-events on failure")

	wg.Add(1)

	tm := time.NewTimer(5 * time.Millisecond)

	go func() {
		<-tm.C

		_ = db.UpsertCheck(cru)
	}()

	go func() {
		dispatchFailureNotification(ctx, db, ce, 100*time.Microsecond, &wg, &fn)
	}()

	wg.Wait()

	ov, fi, up := fn.CountFailures()
	if ov == 0 {
		t.Errorf("\t%s: should have at least one failure notification dispatched: none", failed)
	} else {
		t.Logf("\t%s: should have at least one failure notification dispatched.", succeed)
	}

	if fi != 1 || up < 1 {
		t.Errorf("\t%s: should have one initial event and many updates: %d to %d", failed, fi, up)
	} else {
		t.Logf("\t%s: should have one initial event and many updates", succeed)
	}

	if fn.CountRecoveries() != 1 {
		t.Errorf("\t%s: should have at least one recovery notification dispatched: none", failed)
	} else {
		t.Logf("\t%s: should have at least one recovery notification dispatched.", succeed)
	}
}

func TestDispatchNoUpdateNotification(t *testing.T) {
	db := storage.NewInMemoryDatabase()
	tn := time.Now()
	cr := storage.CheckResult{Success: false, Name: "test", Label: "test", LastUpdate: tn.Add(-1 * time.Minute)}
	cru := storage.CheckResult{Success: true, Name: "test", Label: "test", LastUpdate: tn.Add(2 * time.Minute)}
	ce := CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: cr, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}}
	fn := FakeNotifier{}
	ctx := context.TODO()

	var wg sync.WaitGroup

	_ = db.UpsertCheck(cr)

	t.Log("should be able to generate multiple notification-events on no-update")

	wg.Add(1)

	tm := time.NewTimer(5 * time.Millisecond)

	go func() {
		<-tm.C

		_ = db.UpsertCheck(cru)
	}()

	go func() {
		dispatchNoUpdateNotification(ctx, db, ce, 100*time.Microsecond, &wg, &fn)
	}()

	wg.Wait()

	if fn.CountNoUpdate() == 0 {
		t.Errorf("\t%s: should dispach at least one notification on no-update: %d", failed, fn.CountNoUpdate())
	} else {
		t.Logf("\t%s: should dispatch at least one notification on no-update", succeed)
	}
}

func TestChooseDispatch(t *testing.T) {
	t.Log("should be able to choose the correct dispatch")

	tn := time.Now()
	crs := storage.CheckResult{Success: true, Name: "test", Label: "test", LastUpdate: tn.Add(-1 * time.Minute)}
	crf := storage.CheckResult{Success: false, Name: "test", Label: "test", LastUpdate: tn.Add(-1 * time.Minute)}
	vals := []struct {
		name  string
		event CheckEvent
		exp   func(ctx context.Context, db storage.Database, event CheckEvent, updatePeriod time.Duration, wg *sync.WaitGroup, notifier Notifier)
	}{
		{"no-update-no-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: crs, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: false}, nil},
		{"no-update-with-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crf, New: crs, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: false}, nil},
		{"update-no-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: crs, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: true}, dispatchNoUpdateNotification},
		{"update-with-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crf, New: crs, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: true}, dispatchNoUpdateNotification},
		{"failure-update-no-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: true}, dispatchFailureNotification},
		{"failure-update-with-old-failing-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crf, New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: true}, nil},
		{"failure-update-with-old-failing-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crs, New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: true}, dispatchFailureNotification},
		{"failure-update-no-old-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: false}, dispatchFailureNotification},
		{"failure-update-with-old-failing-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crf, New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: false}, nil},
		{"failure-update-with-old-failing-check", CheckEvent{EventDate: tn.Add(-1 * time.Minute), Old: crs, New: crf, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}, WatchesForUpdates: false}, dispatchFailureNotification},
	}

	for _, tt := range vals {
		t.Run(tt.name, func(t *testing.T) {
			res := chooseDispatch(tt.event)
			if tt.exp == nil && res != nil {
				t.Fatalf("\t%s: should return nil: %s", failed, runtime.FuncForPC(reflect.ValueOf(res).Pointer()).Name())
			} else if tt.exp == nil && res == nil {
				t.Logf("\t%s: should return nil", succeed)

				return
			}

			if res == nil {
				t.Fatalf("\t%s: should not return nil.", failed)
			} else {
				t.Logf("\t%s: should not return nil", succeed)
			}

			resP := reflect.ValueOf(res).Pointer()
			expP := reflect.ValueOf(tt.exp).Pointer()

			if resP != expP {
				t.Errorf("\t%s: should return expected dispatching function %s: %s", failed, runtime.FuncForPC(expP).Name(), runtime.FuncForPC(resP).Name())
			} else {
				t.Logf("\t%s: should return expected dispatching function %s", succeed, runtime.FuncForPC(expP).Name())
			}
		})
	}
}

func TestNotificationDispatcherAsAWhole(t *testing.T) {
	// WARNING: this test should be run with `timeout`-flag. If something does not work, it can run indefinitely
	t.Log("should be able to dispatch several notifications and end itself when context gets canceled")

	db := storage.NewInMemoryDatabase()
	tn := time.Now()
	cr := storage.CheckResult{Success: false, Name: "test", Label: "test", LastUpdate: tn.Add(-1 * time.Minute)}
	ce := CheckEvent{EventDate: tn.Add(-1 * time.Minute), New: cr, Notify: []string{"test@example.org"}, GraceTimeDeadline: time.Time{}, UpdateDeadline: time.Time{}}
	fn := FakeNotifier{}
	ctx, cancel := context.WithCancel(context.TODO())
	_ = db.UpsertCheck(cr)

	var wg sync.WaitGroup

	wg.Add(1)

	checks := make(chan CheckEvent, 1)

	go NotificationDispatcher(ctx, &wg, 100*time.Microsecond, db, checks, &fn)

	checks <- ce

	tmc := time.NewTimer(5 * time.Millisecond)

	go func() {
		<-tmc.C
		cancel()
	}()

	wg.Wait()

	t.Logf("\t%s: should be able to exit on context-cancellation.", succeed)

	overall, _, _ := fn.CountFailures()

	if overall == 0 {
		t.Errorf("\t%s: should register some dispatched events: %d events", failed, overall)
	} else {
		t.Logf("\t%s: should register some dispatched events: %d events", succeed, overall)
	}
}
