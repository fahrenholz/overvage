package storage

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	_ "github.com/joho/godotenv/autoload"
	"github.com/sethvargo/go-envconfig"
)

const (
	succeed = "\u2713"
	failed  = "\u2717"
)

type TestConfig struct {
	MysqlDBHost string `env:"MYSQL_DB_HOST,default=localhost:13306"`
}

//nolint:gochecknoglobals //test config
var tc TestConfig

func init() {
	_ = envconfig.Process(context.TODO(), &tc)
}

func TestNewDatabase(t *testing.T) {
	values := []struct {
		name   string
		config DatabaseConfig
		expErr bool
	}{
		{DBTypeInMemory, DatabaseConfig{Type: "in_memory"}, false},
		{DBTypeMysql, DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)}, false},
		{"error", DatabaseConfig{Type: "inexistant"}, true},
	}

	t.Log("should get a new database according to given configuration")

	for _, tt := range values {
		t.Run(tt.name, func(t *testing.T) {
			if tt.config.Type == "mysql" {
				_, _ = getMysqlTestDBHandle()
			}

			db, err := NewDatabase(tt.config)
			if err != nil {
				if !tt.expErr {
					t.Errorf("\t%s: should not get any error: %v", failed, err)
				} else {
					t.Logf("\t%s: should get an error.", succeed)
				}

				return
			}

			if tt.expErr {
				t.Fatalf("\t%s: should get an error: no error returned", failed)
			} else {
				t.Logf("\t%s: should not get any error.", succeed)
			}

			switch tt.config.Type {
			case DBTypeInMemory:
				switch db.(type) {
				case InMemoryDatabase:
					t.Logf("\t%s: db is of type InMemoryDatabase", succeed)
				default:
					t.Errorf("\t%s: db is of type InMemoryDatabase: %s", failed, reflect.TypeOf(db))
				}
			case DBTypeMysql:
				switch db.(type) {
				case MysqlDatabase:
					t.Logf("\t%s: db is of type MysqlDatabase", succeed)
				default:
					t.Errorf("\t%s: db is of type MysqlDatabase: %s", failed, reflect.TypeOf(db))
				}
			}
		})
	}
}
