package storage

import (
	"sync"
)

// NewInMemoryDatabase creates a ready-to-use InMemoryDatabase-value.
func NewInMemoryDatabase() InMemoryDatabase {
	res := InMemoryDatabase{checks: make(map[string]CheckResult), mux: &sync.Mutex{}}

	return res
}

// InMemoryDatabase represents a not-persisting data-structure implementing Database.
type InMemoryDatabase struct {
	checks map[string]CheckResult
	mux    *sync.Mutex
}

// GetChecks returns all checks currently stored in the InMemoryDatabase-struct.
func (db InMemoryDatabase) GetChecks() ([]CheckResult, error) {
	res := make([]CheckResult, len(db.checks))
	i := 0

	for _, v := range db.checks {
		res[i] = v
	}

	return res, nil
}

// GetCheckByName returns one single check identified by its name and ErrNoCheckResultFound if no check was found.
func (db InMemoryDatabase) GetCheckByName(name string) (CheckResult, error) {
	db.mux.Lock()
	v, ok := db.checks[name]
	db.mux.Unlock()

	if !ok {
		return CheckResult{}, ErrNoCheckResultFound
	}

	return v, nil
}

// UpsertCheck stores a single check into the InMemoryDatabase-struct.
func (db InMemoryDatabase) UpsertCheck(result CheckResult) error {
	db.mux.Lock()
	db.checks[result.Name] = result
	db.mux.Unlock()

	return nil
}

// Close does absolutely nothing, but is needed to satisfy Database as other systems have connections to close.
func (db InMemoryDatabase) Close() error {
	return nil
}
