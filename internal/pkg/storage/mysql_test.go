package storage

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
)

func TestNewMysqlDatabase(t *testing.T) {
	t.Log("should get a new db-handle and apply migrations correctly")

	db, err := getMysqlTestDBHandle()
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	h, err := NewMysqlDatabase(DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)})
	if err != nil {
		t.Fatalf("\t%s: should not return any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error.", succeed)
	}

	err = h.conn.Ping()
	if err != nil {
		t.Errorf("\t%s: connection should be pingable: %v", failed, err)
	} else {
		t.Logf("\t%s: connection should be pingable.", succeed)
	}

	r := db.QueryRow("SELECT * from schema_migrations")
	if r == nil {
		t.Fatalf("\t%s: should have one row in schema_migrations: nil found", failed)
	} else {
		t.Logf("\t%s: should have one row in schema-migrations.", succeed)
	}

	var version, dirty int

	_ = r.Scan(&version, &dirty)

	if version < 1 {
		t.Errorf("\t%s: expect migration version greater than 0: version is %d", failed, version)
	} else {
		t.Logf("\t%s: expect migration version greater than 0: version is %d", succeed, version)
	}
}

func TestMysqlDatabase_UpsertCheck(t *testing.T) {
	t.Log("should be able to insert a check into a mysql database")

	n := time.Now().Round(time.Second).UTC()
	exp := CheckResult{Name: "testcheck", Label: "Test check", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}

	db, err := getMysqlTestDBHandle()
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	h, _ := NewMysqlDatabase(DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)})

	for i := 1; i < 3; i++ {
		err = h.UpsertCheck(exp)
		if err != nil {
			t.Errorf("\t%s: insert %d: should not return any errors: %v", failed, i, err)
		} else {
			t.Logf("\t%s: insert %d: should not return any errors.", succeed, i)
		}

		r, err := db.Queryx("SELECT * FROM overvage_checks WHERE name = 'testcheck'")
		if err != nil {
			t.Errorf("\t%s: insert %d: query should not return any errors: %v", failed, i, err)
		} else {
			t.Logf("\t%s: insert %d: query should not return any errors", succeed, i)
		}

		var res CheckResult

		cnt := 0

		for r.Next() {
			err := r.StructScan(&res)
			if err != nil {
				t.Errorf("\t%s: insert %d: should be scannable: %v", failed, i, err)
			} else {
				t.Logf("\t%s: insert %d: should be scannable.", succeed, i)
			}

			if !reflect.DeepEqual(exp, res) {
				t.Errorf("\t%s: insert %d: retrieved check should be as expected: (res: %v, exp: %v)", failed, i, res, exp)
			} else {
				t.Logf("\t%s: insert %d: retrieved check should be as expected.", succeed, i)
			}

			cnt++
		}

		if cnt != 1 {
			t.Errorf("\t%s: should have exactly one row after %d inserts: %d", failed, i, cnt)
		} else {
			t.Logf("\t%s: should have exactly one row after %d inserts.", succeed, i)
		}

		//nolint:sqlclosecheck
		_ = r.Close()
	}
}

func TestMysqlDatabase_GetChecks(t *testing.T) {
	t.Log("should be able to return current checks from the database")

	n := time.Now().Round(time.Second).UTC()
	exp1 := CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}

	_, err := getMysqlTestDBHandle()
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	h, _ := NewMysqlDatabase(DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)})

	err = h.UpsertCheck(exp1)
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	err = h.UpsertCheck(exp2)
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	ch, err := h.GetChecks()
	if err != nil {
		t.Errorf("\t%s: should not generate any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not generate any error.", succeed)
	}

	var foundExp1, foundExp2 bool

	for _, v := range ch {
		if reflect.DeepEqual(exp1, v) {
			foundExp1 = true
		}

		if reflect.DeepEqual(exp2, v) {
			foundExp2 = true
		}
	}

	if !foundExp1 {
		t.Errorf("\t%s: expect first check to be retrieved: not found in %v", failed, ch)
	} else {
		t.Logf("\t%s: expect first check to be retrieved.", succeed)
	}

	if !foundExp2 {
		t.Errorf("\t%s: expect second check to be retrieved: not found in %v", failed, ch)
	} else {
		t.Logf("\t%s: expect second check to be retrieved.", succeed)
	}
}

func TestMysqlDatabase_GetCheckByName(t *testing.T) {
	t.Log("should be able to retrieve check by name or generate error")

	n := time.Now().Round(time.Second).UTC()
	exp1 := CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}
	vals := []struct {
		name        string
		set         []CheckResult
		search      string
		expNoResult bool
	}{
		{"found-in-2-pieces-set", []CheckResult{exp1, exp2}, exp1.Name, false},
		{"found-in-1-piece-set", []CheckResult{exp1}, exp1.Name, false},
		{"not-found-in-2-pieces-set", []CheckResult{exp1, exp2}, "doesnotexist", true},
	}

	for _, tt := range vals {
		t.Run(tt.name, func(t *testing.T) {
			_, err := getMysqlTestDBHandle()
			if err != nil {
				t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
			}

			h, _ := NewMysqlDatabase(DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)})

			for _, sV := range tt.set {
				_ = h.UpsertCheck(sV)
			}

			res, err := h.GetCheckByName(tt.search)
			if tt.expNoResult {
				if !errors.Is(err, ErrNoCheckResultFound) {
					t.Errorf("\t%s: should return ErrNoCheckResultFound-error when nothing was found: %v", failed, err)
				} else {
					t.Logf("\t%s: should return ErrNoCheckResultFound-error when nothing was found.", succeed)
				}

				return
			}

			if err != nil {
				t.Errorf("\t%s: should not throw any error: %v", failed, err)
			} else {
				t.Logf("\t%s: should not throw any error.", succeed)
			}

			if !reflect.DeepEqual(res, exp1) {
				t.Errorf("\t%s: should find check according to name: %v", failed, res)
			} else {
				t.Logf("\t%s: should find check according to name.", succeed)
			}
		})
	}
}

func TestMysqlDatabase_Close(t *testing.T) {
	t.Log("should be able to close the database connection without error")

	_, err := getMysqlTestDBHandle()
	if err != nil {
		t.Fatalf("\t%s: (preparation) could not prepare test database: %v", failed, err)
	}

	h, _ := NewMysqlDatabase(DatabaseConfig{Type: "mysql", ConnectionString: fmt.Sprintf("overvage:overvage@tcp(%s)/overvagetdb?parseTime=true", tc.MysqlDBHost)})

	err = h.Close()
	if err != nil {
		t.Errorf("\t%s: should not return any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error.", succeed)
	}
}

func getMysqlTestDBHandle() (*sqlx.DB, error) {
	db, err := sqlx.Open("mysql", fmt.Sprintf("root:lala@tcp(%s)/?parseTime=true", tc.MysqlDBHost))
	if err != nil {
		return nil, err
	}

	_, err = db.Exec("USE overvagetdb")
	if err != nil && err.Error() != "Error 1049: Unknown database 'overvagetdb'" {
		return nil, err
	}

	if err == nil {
		_, err = db.Exec("DROP DATABASE overvagetdb")
		if err != nil {
			return nil, err
		}
	}

	_, err = db.Exec("CREATE DATABASE overvagetdb")
	if err != nil {
		return nil, err
	}

	_, err = db.Exec("GRANT ALL privileges ON overvagetdb.* TO overvage")
	if err != nil {
		return nil, err
	}

	_, err = db.Exec("USE overvagetdb")
	if err != nil {
		return nil, err
	}

	return db, nil
}
