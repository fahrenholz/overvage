package storage

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

const (
	DBTypeMysql    = "mysql"
	DBTypeInMemory = "in_memory"
)

var ErrUnknownDBType = errors.New("unknown database type requested")

// CheckResult represents the database-stored elements of a check sent to overvage.
type CheckResult struct {
	Name        string    `db:"name"` // This will only be set by overvage
	Label       string    `json:"label" db:"label"`
	Type        string    `json:"type" db:"type"`
	Description string    `json:"description" db:"description"`
	Priority    int       `json:"priority" db:"priority"`
	LastUpdate  time.Time `db:"last_update"` // This will only be set by overvage
	Referrer    string    `json:"referrer" db:"referrer"`
	Success     bool      `json:"success" db:"success"`
}

// DisplayedTypeGroup transforms the type group of a check into the display-format of overvages ui.
func (c CheckResult) DisplayedTypeGroup() string {
	spl := strings.Split(c.Type, "_")
	gr := "UNKNOWN"

	if len(spl) != 0 && spl[0] != "" {
		gr = spl[0]
	}

	return strings.ToUpper(gr)
}

// DisplayedLastUpdate transforms the `LastUpdate` field of a check into the display-format of overvages ui.
func (c CheckResult) DisplayedLastUpdate() string {
	return c.LastUpdate.Format(time.RFC1123)
}

// ErrNoCheckResultFound is thrown when there is no matching check-result found in database.
var ErrNoCheckResultFound = errors.New("no check result found")

// DatabaseConfig represents the db-specific configuration of overvage.
type DatabaseConfig struct {
	Type             string `env:"DB_TYPE,required"`
	ConnectionString string `env:"DB_CONNECTION_STRING"`
}

// Database is used to decouple multiple persistence-backends able to store check-results.
type Database interface {
	GetChecks() ([]CheckResult, error)
	UpsertCheck(result CheckResult) error
	GetCheckByName(name string) (CheckResult, error)
	Close() error
}

// NewDatabase returns a concrete type implementing Database according to the given configuration.
func NewDatabase(conf DatabaseConfig) (Database, error) {
	switch conf.Type {
	case DBTypeInMemory:
		return NewInMemoryDatabase(), nil
	case DBTypeMysql:
		return NewMysqlDatabase(conf)
	default:
		return nil, fmt.Errorf("%w: %s", ErrUnknownDBType, conf.Type)
	}
}
