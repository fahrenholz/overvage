package storage

import (
	"errors"
	"fmt"

	// mysql is the mysql-driver.
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"

	// pkger is used to compile assets.
	_ "github.com/golang-migrate/migrate/v4/source/pkger"
	"github.com/jmoiron/sqlx"
)

// MysqlDatabase stores the connection to a mysql-db and satisfies Database.
type MysqlDatabase struct {
	conn *sqlx.DB
}

// GetChecks returns all checks currently stored in the database.
func (db MysqlDatabase) GetChecks() ([]CheckResult, error) {
	var cr []CheckResult

	rows, err := db.conn.Queryx("SELECT * FROM overvage_checks")
	if err != nil {
		return nil, fmt.Errorf("error retrieving checks: %w", err)
	}

	defer rows.Close()

	for rows.Next() {
		var res CheckResult

		err = rows.StructScan(&res)
		if err != nil {
			return nil, fmt.Errorf("error scanning checks: %w", err)
		}

		cr = append(cr, res)
	}

	return cr, nil
}

// UpsertCheck inserts one check-result into the database.
func (db MysqlDatabase) UpsertCheck(result CheckResult) error {
	success := 0
	if result.Success {
		success = 1
	}

	_, err := db.conn.NamedExec(
		"INSERT INTO overvage_checks (name, label, type, description, priority, last_update, referrer, success)"+
			"VALUES (:name, :label, :type,:description, :priority, :last_update, :referrer, :success)"+
			"ON DUPLICATE KEY UPDATE label = :label, type = :type, description = :description, priority = :priority, last_update = :last_update, referrer = :referrer, success = :success",
		map[string]interface{}{
			"name":        result.Name,
			"label":       result.Label,
			"type":        result.Type,
			"description": result.Description,
			"priority":    result.Priority,
			"last_update": result.LastUpdate,
			"referrer":    result.Referrer,
			"success":     success,
		})
	if err != nil {
		return fmt.Errorf("[DB] UpsertCheck: %w", err)
	}

	return nil
}

// GetCheckByName returns a check identified by it's name and ErrNoCheckResultFound if no result was found.
func (db MysqlDatabase) GetCheckByName(name string) (CheckResult, error) {
	var cr CheckResult

	row := db.conn.QueryRowx("SELECT * FROM overvage_checks WHERE name = ?", name)

	if err := row.StructScan(&cr); err != nil {
		return CheckResult{}, ErrNoCheckResultFound
	}

	return cr, nil
}

// Close closes the effective database-connection.
func (db MysqlDatabase) Close() error {
	//nolint: wrapcheck
	return db.conn.Close()
}

// NewMysqlDatabase creates a database-connection to mysql and applies all migrations which weren't already applied.
func NewMysqlDatabase(c DatabaseConfig) (MysqlDatabase, error) {
	db, err := sqlx.Open("mysql", c.ConnectionString)
	if err != nil {
		return MysqlDatabase{}, fmt.Errorf("sql: db-opening: %w", err)
	}

	err = db.Ping()
	if err != nil {
		return MysqlDatabase{}, fmt.Errorf("sql: db-ping: %w", err)
	}

	driver, err := mysql.WithInstance(db.DB, &mysql.Config{})
	if err != nil {
		return MysqlDatabase{}, fmt.Errorf("sql: get driver for migration: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance("pkger:///db/migrations/mysql", "mysql", driver)
	if err != nil {
		return MysqlDatabase{}, fmt.Errorf("sql: create migration handle: %w", err)
	}

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return MysqlDatabase{}, fmt.Errorf("sql: execute migration: %w", err)
	}

	return MysqlDatabase{conn: db}, nil
}
