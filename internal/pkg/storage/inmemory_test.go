package storage

import (
	"reflect"
	"testing"
	"time"
)

func TestInMemoryDatabase_UpsertCheck(t *testing.T) {
	t.Log("should be able to insert checks if not present and update")

	n := time.Now().Round(time.Second).UTC()
	exp1 := CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}
	db := NewInMemoryDatabase()

	err := db.UpsertCheck(exp1)
	if err != nil {
		t.Errorf("\t%s: should not return any error on first insert: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error on first insert.", succeed)
	}

	if len(db.checks) != 1 {
		t.Errorf("\t%s: should have one element stored after first insert: %d", failed, len(db.checks))
	} else {
		t.Logf("\t%s: should have one element stored after first insert", succeed)
	}

	err = db.UpsertCheck(exp2)
	if err != nil {
		t.Errorf("\t%s: should not return any error on second insert: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error on second insert.", succeed)
	}

	if len(db.checks) != 2 {
		t.Errorf("\t%s: should have two elements stored after second insert: %d", failed, len(db.checks))
	} else {
		t.Logf("\t%s: should have two elements stored after second insert", succeed)
	}

	err = db.UpsertCheck(exp1)
	if err != nil {
		t.Errorf("\t%s: should not return any error on update: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error on update.", succeed)
	}

	if len(db.checks) != 2 {
		t.Errorf("\t%s: should have two elements stored after update: %d", failed, len(db.checks))
	} else {
		t.Logf("\t%s: should have two elements stored after update", succeed)
	}
}

func TestInMemoryDatabase_GetChecks(t *testing.T) {
	t.Log("should be able to retrieve an array of checks")

	n := time.Now().Round(time.Second).UTC()
	exp1 := CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}
	db := NewInMemoryDatabase()

	_ = db.UpsertCheck(exp1)
	_ = db.UpsertCheck(exp2)

	chs, err := db.GetChecks()
	if err != nil {
		t.Errorf("\t%s: should not return any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error.", succeed)
	}

	if len(chs) != len(db.checks) {
		t.Errorf("\t%s: should return all checks provided to db as slice: %v", failed, chs)
	} else {
		t.Logf("\t%s: should return all checks provided to db as slice", succeed)
	}
}

func TestInMemoryDatabase_GetCheckByName(t *testing.T) {
	t.Log("should be able to retrieve single check by field 'name'")

	n := time.Now().Round(time.Second).UTC()
	exp1 := CheckResult{Name: "testcheck1", Label: "Test check 1", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: true}
	exp2 := CheckResult{Name: "testcheck2", Label: "Test check 2", Type: "http_ping", Description: "test desc", Priority: 1, LastUpdate: n, Referrer: "test", Success: false}
	db := NewInMemoryDatabase()

	_ = db.UpsertCheck(exp1)
	_ = db.UpsertCheck(exp2)

	ch, err := db.GetCheckByName("testcheck1")
	if err != nil {
		t.Errorf("\t%s: should not return any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not return any error", succeed)
	}

	if !reflect.DeepEqual(ch, exp1) {
		t.Errorf("\t%s: should retrieve the correct check: %v", failed, ch)
	} else {
		t.Logf("\t%s: should retrieve the correct check.", succeed)
	}
}

func TestInMemoryDatabase_Close(t *testing.T) {
	t.Log("should be able to execute close. InmemoryDb just has it for interface-compliance though")

	db := NewInMemoryDatabase()

	err := db.Close()
	if err != nil {
		t.Errorf("\t%s: should not generate any error: %v", failed, err)
	} else {
		t.Logf("\t%s: should not generate any error", succeed)
	}
}
