GOCMD=go
GOOS    ?= $(shell $(GO) env GOOS)
GOARCH  ?= $(shell $(GO) env GOARCH)
GOHOST  ?= GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOCMD)

# Build variables
VERSION 	?= $(shell git describe --tags --always)

# Go variables
GO      ?= go
GOOS    ?= $(shell $(GO) env GOOS)
GOARCH  ?= $(shell $(GO) env GOARCH)
GOHOST  ?= GOOS=$(GOOS) GOARCH=$(GOARCH) $(GO)

LDFLAGS ?= "-X main.version=$(VERSION)"

.PHONY: setup
setup: setup-ci				## Prepare your local machine for development
	pre-commit install

.PHONY: setup-ci
setup-ci:					## Prepares the ci environment
	go install gotest.tools/gotestsum@v1.6.4
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.39.0
	go install github.com/itchyny/gojq/cmd/gojq@v0.12.4

.PHONY: compose-up
compose-up: 				## Start dev-environment
	docker-compose up -d

.PHONY: compose-down
compose-down: 				## Stop dev-environment
	docker-compose down -d

.PHONY: package-assets
package-assets:				## Package all assets with pkger
	pkger

.PHONY: run
run: package-assets			## Run overvage with dev defaults
	go run .

.PHONY: build
build:						## Build overvage-binary for current host
	CGO_ENABLED=0 $(GOHOST) build -ldflags=$(LDFLAGS) -o overvage.bin --mod=vendor .

.PHONY: test
test: 						## Test all packages from overvage
	gotestsum --no-summary=skipped --jsonfile testresult.json --format standard-verbose -- -timeout 1m -coverprofile=cov.out -failfast -race ./...

.PHONY: lint
lint:
	golangci-lint run

.PHONY: help
help:   					## Display this help
	@awk \
		-v "col=\033[36m" -v "nocol=\033[0m" \
		' \
			BEGIN { \
				FS = ":.*##" ; \
				printf "Usage:\n  make %s<target>%s\n", col, nocol \
			} \
			/^[a-zA-Z_-]+:.*?##/ { \
				printf "  %s%-12s%s %s\n", col, $$1, nocol, $$2 \
			} \
			/^##@/ { \
				printf "\n%s%s%s\n", nocol, substr($$0, 5), nocol \
			} \
		' $(MAKEFILE_LIST)

.PHONY: log
log-%:
	@grep -h -E '^$*:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk \
			'BEGIN { \
				FS = ":.*?## " \
			}; \
			{ \
				printf "\033[36m==> %s\033[0m\n", $$2 \
			}'